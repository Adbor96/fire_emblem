﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreadthAgent : MonoBehaviour
{
    private PathFinderGrid grid;
    public bool selected;
    private bool nodesChecked;
    [SerializeField]
    private int movement;
    private int xPos, yPos;
    public int Movement
    {
        get
        {
            return movement;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        grid = FindObjectOfType<PathFinderGrid>();
    }

    // Update is called once per frame
    void Update()
    {
        if (selected && !nodesChecked)
        {
            xPos = (int)transform.position.x;
            yPos = (int)transform.position.y;
            grid.GetList(xPos, yPos, movement);
            nodesChecked = true;
        }
        if (!selected)
        {
            nodesChecked = false;
            grid.Deselect();
        }
    }
}
