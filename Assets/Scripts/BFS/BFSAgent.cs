﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BFSAgent : MonoBehaviour
{
    public int movement;
    public Dictionary<Vector3, bool> walkablePosition;
    public bool selected;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (selected)
        {
            for (int x = -movement; x <= movement; x++)
            {
                //få tag i tiles längs y axeln
                //get the tiles along the y axis
                for (int y = -movement; y <= movement; y++)
                {

                }
            }
        }
    }
    //get the nearest walkable nodes
    List<Vector3> GetWalkableNodes(Vector3 currentPosition)
    {
        List<Vector3> walkableNodes = new List<Vector3>();
        List<Vector3> possibleNodes = new List<Vector3>()
        {
            new Vector3(currentPosition.x + 1, currentPosition.y),
            new Vector3(currentPosition.x - 1, currentPosition.y),
            new Vector3(currentPosition.x, currentPosition.y + 1),
            new Vector3(currentPosition.x, currentPosition.y - 1),
        };
        foreach (Vector3 node in possibleNodes)
        {
            walkableNodes.Add(node);
        }
        return walkableNodes;
    }
    Vector3 GetNodesBFS(Vector3 startPos, Vector3 endPos)
    {
        Queue<Vector3> queue = new Queue<Vector3>();
        HashSet<Vector3> exploredNodes = new HashSet<Vector3>();
        queue.Enqueue(startPos);
        while(queue.Count != 0)
        {
            Vector3 currentNode = queue.Dequeue();
            if(currentNode == endPos)
            {
                return currentNode;
            }
            List<Vector3> nodes = GetWalkableNodes(currentNode);
            foreach (Vector3 node in nodes)
            {
                if (!exploredNodes.Contains(node))
                {
                    //mark the node as explored
                    exploredNodes.Add(node);

                    //store a reference to the previous node

                    //add this to the queue of nodes to examine
                    queue.Enqueue(node);
                }
            }
        }
        return startPos;
    }
}
