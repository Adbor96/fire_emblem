﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "AdamTile", menuName = "AdamTiles")]
public class AdamTile : Tile
{
    public int moveCost;
    public bool walkable;
    public Sprite customSprite
    {
        get
        {
            return sprite;
        }
        set
        {

        }
    }
    public override void GetTileData(Vector3Int position, ITilemap tilemap, ref TileData tileData)
    {
        bool evenCell = Mathf.Abs(position.y + position.x) % 2 > 0;
        tileData.sprite = customSprite;
    }
}
