﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[System.Serializable]
public class PathFinderNode 
{
    public bool walkable;
    public int xPos, yPos;
    public int moveCost = 1;
    private int newMoveCost;
    public int agentMovePointSpent = 1000;
    public PathFinderNode(bool _isWalkable, int _moveCost)
    {
        walkable = _isWalkable;
        moveCost = _moveCost;
        //Debug.Log("Node created");
    }
}
public class PathFinderGrid: MonoBehaviour
{
    private PathFinderNode[,] gridNodes;
    private PathFinderNode currentNode;
    public Tilemap tileMap; // the tilemap the nodes are based on
    public bool drawn; //whether to draw the debug
    public List<PathFinderNode> reachableDebugList; //list to debug which nodes can be reached
    private List<PathFinderNode> affectedNodes = new List<PathFinderNode>(); //the nodes that have been affected and then need to be cleared
    private void Awake()
    {
        //Debug.Log("Tilemap width: " + tileMap.size.x);
        //Debug.Log("Tilemap height: " + tileMap.size.y);
        gridNodes = new PathFinderNode[tileMap.size.x, tileMap.size.y];
        for (int x = 0; x < tileMap.size.x; x++)
        {
            for (int y = 0; y < tileMap.size.y; y++)
            {
                if(tileMap.GetTile(new Vector3Int(x, y, 0)) != null)
                {
                    Debug.Log(tileMap.GetTile(new Vector3Int(x, y, 0)).name);
                    //Debug.Log(tileMap.GetTile(new Vector3Int(x, y, 0)));
                    TileBase testTile = tileMap.GetTile(new Vector3Int(x, y, 0));
                    if(testTile is AdamTile)
                    {
                        AdamTile testAdamTile = (AdamTile)testTile;
                        Debug.Log("AdamTile move cost = " + testAdamTile.moveCost);
                        gridNodes[x, y] = new PathFinderNode(testAdamTile.walkable, testAdamTile.moveCost);
                    }
                    else
                    {
                        Debug.LogError("Tile was not an Adam tile" + x + ", " + y);
                    }
                }
                else
                {
                    //Debug.Log("Set out non walkable nodes");
                    gridNodes[x, y] = new PathFinderNode(false, 1000);
                }
                gridNodes[x, y].xPos = x; //set the position of the new node on the x position
                gridNodes[x, y].yPos = y; //set the position of the new node on the y position
            }
        }
        //reachableDebugList = ReachableNodes(8, 8, 10);
    }
    private List<PathFinderNode> Neighbours(PathFinderNode startNode)
    {
        //get the neighbours from the start node
        List<PathFinderNode> neighbours = new List<PathFinderNode>();
        PathFinderNode westNeighbour = GetNodeAtPosition(startNode.xPos - 1, startNode.yPos);
        PathFinderNode eastNeighbour = GetNodeAtPosition(startNode.xPos + 1, startNode.yPos);
        PathFinderNode northNeighbour = GetNodeAtPosition(startNode.xPos, startNode.yPos + 1);
        PathFinderNode southNeighbour = GetNodeAtPosition(startNode.xPos, startNode.yPos - 1);
        if(westNeighbour != null)
        neighbours.Add(westNeighbour);
        if(eastNeighbour != null)
        neighbours.Add(eastNeighbour);
        if (northNeighbour != null)
            neighbours.Add(northNeighbour);
        if(southNeighbour != null)
        neighbours.Add(southNeighbour);
        return neighbours;
    }
    private PathFinderNode GetNodeAtPosition(int x, int y)
    {
        if(x < 0 || y < 0 || x >= gridNodes.GetLength(0) || y >= gridNodes.GetLength(1))
        {
            return null;
        }
        return gridNodes[x, y];
    }
    //the nodes the player can reach
    public List<PathFinderNode> ReachableNodes(int xPos, int yPos, int totalMovePoints)
    {
        List<PathFinderNode> confirmedReachable = new List<PathFinderNode>(); //the nodes you CAN reach
        confirmedReachable.Clear();
        Debug.Log("Clear reachable nodes");
       // List<PathFinderNode> closedNodes = new List<PathFinderNode>(); //nodes that have been checked and will not be checked again
        Queue<PathFinderNode> openNodes = new Queue<PathFinderNode>(); //nodes that have not been checked yet
        openNodes.Clear();
        Debug.Log("Clear open nodes");
        GetNodeAtPosition(xPos, yPos).agentMovePointSpent = 0; //reset the startnodes move points spent
        affectedNodes.Add(GetNodeAtPosition(xPos, yPos));
        openNodes.Enqueue(GetNodeAtPosition(xPos, yPos)); //add the start node to the open nodes
        //add the neighbour nodes to the open nodes
        while (openNodes.Count != 0)
        {
            //Debug.Log("Check the open nodes");
            //check the enqueued nodes and check if they are reachable
            currentNode = openNodes.Dequeue();
            if(currentNode.agentMovePointSpent <= totalMovePoints)
            {
                if (!confirmedReachable.Contains(currentNode)) //check if the current node is not at the start node
                {
                    if (currentNode.xPos != xPos || currentNode.yPos != yPos) //do not have the start position as a walkable node
                    {
                        //if the node is not already in the list of reachable nodes, add the node to the list
                        confirmedReachable.Add(currentNode);
                        //Debug.Log("Add new reachable node");
                    }
                }
            }
            foreach (PathFinderNode node in Neighbours(currentNode))
            {
                if (!node.walkable)
                {
                    continue; //do not check the neighbour node if it is not walkable
                }
                int newMovePointsSpent = currentNode.agentMovePointSpent + node.moveCost; //check how many move points the agent has spent reachign the new node
                if (newMovePointsSpent < node.agentMovePointSpent)
                {
                    //correct the move points spent getting to a new node
                    if (!openNodes.Contains(node))
                    {
                        if (currentNode.agentMovePointSpent <= totalMovePoints)
                        {
                            openNodes.Enqueue(node); //only add the new node if it has a better spending of move points and it is not already in the queue of open nodes
                            Debug.Log("Enqueue the open node");
                        }
                    }
                    node.agentMovePointSpent = newMovePointsSpent;
                    affectedNodes.Add(node);
                }

            }
        }
        foreach (PathFinderNode affectedNode in affectedNodes) 
        {
            affectedNode.agentMovePointSpent = 1000;
        }
        affectedNodes.Clear(); //clear the affected nodes
        return confirmedReachable;
        //Queue<Vector3> queue = new Queue<Vector3>();
        //List<Vector3> exploredNodes = new List<Vector3>();
    }
    private void OnDrawGizmos()
    {
        //Debug.Log("Draw gizmos");
        /*if(gridNodes != null && drawn)
        {
            for (int x = 0; x < gridNodes.GetLength(0); x++)
            {
                for (int y = 0; y < gridNodes.GetLength(1); y++)
                {
                    if (gridNodes[x, y].walkable)
                    {
                        Debug.Log("Draw cubes");
                        Gizmos.color = Color.red;
                        Gizmos.DrawCube(new Vector3(x + 0.5f, y + 0.5f, 0), new Vector3(0.5f, 0.5f, 0.5f));
                    }
                }
            }
        }*/
        if(reachableDebugList != null)
        {
            foreach (PathFinderNode node in reachableDebugList)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawCube(new Vector3(node.xPos + 0.5f, node.yPos + 0.5f, 0), Vector3.one);
            }
        }
    }
    public void GetList(int Xpos, int Ypos, int movePoints)
    {
        reachableDebugList = ReachableNodes(Xpos, Ypos, movePoints);
    }
    public void Deselect()
    {
        reachableDebugList.Clear();
    }
}
