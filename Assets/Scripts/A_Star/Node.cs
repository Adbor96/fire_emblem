﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node 
{
    public bool walkable;
    public Vector3 worldPosition;

    public int gCost;
    public int hCost;
    public Node parent;

    public int gridX;
    public int gridY;


    public Node(bool _Walkable, Vector3 _WorldPos, int _gridX, int _gridY)
    {
        walkable = _Walkable;
        worldPosition = _WorldPos;
        gridX = _gridX;
        gridY = _gridY;
    }
    public int fCost
    {
        get
        {
            return gCost + hCost;
        }
    }
}
