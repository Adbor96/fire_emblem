﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundClass : UnitClass
{
    private void Awake()
    {
        _movType = MovementType.ground; //denna typ av enhet går på marken
    }
    public enum classType
    {
        infantry,
        armor,
        cavalry
    }
    public LayerMask[] walkable; //denna typ av mark kan enheten gå på
    public classType _classType;
}
