﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MovementType
{
    ground,
    flier
}
public class UnitClass : ScriptableObject
{
    public string unitName; //namnet på karaktären
    public string className; //namnet på klassen
    public int movement; //hur långt karaktären kan gå

    public Stats hp, str, mag, spd, def, res;
    public int level;
    public GameObject prefab; //den sprite karaktären har
    public MovementType _movType;
    public weaponType[] useableWeapons;
}
