﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfantryClass : GroundClass
{
    private void Awake()
    {
        _classType = classType.infantry;
    }
    public enum infantryClass
    {
        soldier,
        myrmidon,
        brigand,
        archer
    }
}
