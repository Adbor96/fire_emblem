﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class UnitController : MonoBehaviour
{
    [SerializeField]
    private UnitClass unitData;
    private Tilemap walkable;

    private Vector2 lastPosition; //den senaste rutan spelaren var på

    private List<Vector2> lastPositions;
    private int currentMovementLength; //hur många rutor spelaren har rört sig
    private Vector2 currentPosition; //rutan spelaren är på just nu

    public bool isSelected;

    Vector3 startingPosition;
    public Color selectColor;
    // Start is called before the first frame update
    void Start()
    {
        walkable = FindObjectOfType<Grid>().transform.GetChild(0).GetComponent<Tilemap>();
        /* Debug.Log("Str: " + objectInfo.str.GetValue());
         Debug.Log("Mag: " + objectInfo.mag.GetValue());
         Debug.Log("Spd: " + objectInfo.spd.GetValue());
         Debug.Log("Def: " + objectInfo.def.GetValue());
         Debug.Log("Res: " + objectInfo.res.GetValue());
         Debug.Log("HP: " + objectInfo.hp.GetValue());*/
        lastPosition = transform.position;
        lastPositions = new List<Vector2>();
        lastPositions.Add(transform.position);

        startingPosition = walkable.WorldToCell(transform.position);
    }

    // Update is called once per frame
    void Update()
    {
       // x = Input.GetAxisRaw("Horizontal");
        //y = Input.GetAxisRaw("Vertical");

        ///Move(new Vector2(x, y));
        ///
        if (Input.GetKeyDown(KeyCode.W))
        {
            Move(new Vector2(0, 1));
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            Move(new Vector2(0, -1));
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            Move(new Vector2(-1, 0));
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            Move(new Vector2(1, 0));
        }
        if (isSelected)
        {
            Vector3Int gridPosition = walkable.WorldToCell(transform.position);

            /*for (int x = 0; x == objectInfo.movement; x++)
            {
                //gå igenom varje ruta från där spelaren står
                for (int y = 0; y < objectInfo.movement; y++)
                {
                    walkable.GetTile(gridPosition + new Vector3Int(x, 0, 0));
                    walkable.GetTile(new Vector3Int(0, x, 0) + new Vector3Int(0, y, 0));
                    walkable.SetColor(gridPosition, Color.blue);

                }

            }*/

            //for loop för att gå igenom alla rutor spelaren kan röra sig genom
            //for loop to go through all the spaces the unit can move through

            //få tag i tiles längs x axeln
            //get the tiles along the x axis

            for (int x = -unitData.movement; x <= unitData.movement; x++)
            {
                //få tag i tiles längs y axeln
                //get the tiles along the y axis
                for (int y = -unitData.movement; y <= unitData.movement; y++)
                {
                    
                    if(Mathf.Abs(x) + Mathf.Abs(y) <= unitData.movement)
                    {
                        //för varje tile som valdes, gör den blå
                        //for every tile selected, make it blue
                        Vector3Int walkablePosition = walkable.WorldToCell(startingPosition + new Vector3(x, y));
                            walkable.SetColor(walkablePosition, selectColor);
                    }
                }
            }
            //Vector3Int defaultPos = walkable.WorldToCell(startingPosition);
            //walkable.SetColor(defaultPos, Color.white);
        }
    }
    void Move(Vector2 direction)
    {
        if (CanMove(direction))
        {
            Vector2 destination;
            //spara vart spelaren kommer hamna
            destination = transform.position + (Vector3)direction;

            //om spelaren inte kommer hamna på den senaste platsen den var på
            if (destination != lastPositions[lastPositions.Count - 1])
            {
                //om spelaren inte har rört sig fler steg än den kan under en runda
                if (currentMovementLength < unitData.movement)
                {
                    //spara spelarens senaste position
                    //lastPosition = transform.position;
                    lastPositions.Add(transform.position);
                    transform.position += (Vector3)direction;
                    //spara spelarens nya position
                    currentPosition = transform.position;
                    //om spelaren inte kommer hamna på den senaste platsen, har den gått ett steg
                    currentMovementLength++;
                }

            }
            //om spelaren kommer hamna på den senaste platsen den var på
            else
            {
                //spara spelarens senaste position
                //lastPosition = transform.position;
                Vector3Int gridPosition = walkable.WorldToCell(currentPosition);
                lastPositions.RemoveAt(lastPositions.Count - 1);
                transform.position += (Vector3)direction;
                //spara spelarens nya position
                currentPosition = transform.position;
                //om spelaren hamnar på den senaste platsen har den gått tillbaka ett steg
                currentMovementLength--;
                walkable.SetColor(gridPosition, Color.white);
                Vector3Int playerPosition = walkable.WorldToCell(transform.position);
                walkable.SetColor(playerPosition, Color.white);
            }
        }
    }
    private bool CanMove(Vector2 direction)
    {
        Vector3Int gridPosition = walkable.WorldToCell(transform.position + (Vector3)direction);
        if (!walkable.HasTile(gridPosition))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
