﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Axe Weapon", menuName = "Weapons/Melee/Axe")]
public class Axe : Weapon
{
    private void Awake()
    {
        damageType = weaponDamageType.melee;
        type = weaponType.axe;
    }
}
