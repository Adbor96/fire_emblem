﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Sword Weapon", menuName = "Weapons/Melee/Sword")]
public class Sword : Weapon
{
    private void Awake()
    {
        damageType = weaponDamageType.melee;
        type = weaponType.sword;
    }
}
