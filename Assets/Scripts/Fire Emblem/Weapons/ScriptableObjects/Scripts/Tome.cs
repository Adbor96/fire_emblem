﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Magic Weapon", menuName = "Weapons/Magic/Tome")]
public class Tome : Weapon
{
    private void Awake()
    {
        damageType = weaponDamageType.magical;
        type = weaponType.tome;
    }
}
