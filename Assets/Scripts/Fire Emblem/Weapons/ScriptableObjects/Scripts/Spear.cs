﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Spear Weapon", menuName = "Weapons/Melee/Spear")]
public class Spear : Weapon
{
    private void Awake()
    {
        damageType = weaponDamageType.melee;
        type = weaponType.lance;
    }
}
