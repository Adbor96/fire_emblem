﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum weaponDamageType
{
    melee,
    magical
}
public enum weaponType
{
    sword, 
    lance,
    axe,
    bow,
    tome
}
public abstract class Weapon : ScriptableObject
{
    public int might; //hur mycket vapnet lägger till på karaktärens strength eller magic
    public string weaponName; //namnet på vapnet
    public GameObject prefab;
    public weaponDamageType damageType;
    public weaponType type;
    public int durability;
    public int range; //hur långt vapnet når. 1 är melee, 2 är ranged
}
