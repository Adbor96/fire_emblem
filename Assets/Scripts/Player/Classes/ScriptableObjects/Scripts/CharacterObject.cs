﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public enum movementType
{
    infantry,
    cavalier,
    armor,
    flier
}
public abstract class CharacterObject : ScriptableObject
{
    public GameObject prefab;
    public movementType type;

    public Dictionary<string, int> stats = new Dictionary<string, int>();
    public int movement;

    private void Awake()
    {

    }
}
