﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CharacterMovement : MonoBehaviour
{
    public int movementLimit; //hur många rutor spelaren kan röra sig
    private int currentMovementLength; //hur många rutor spelaren har rört sig
    public bool playerSelected; //om spelaren är vald och kan röra sig
    private Vector2 currentPosition; //rutan spelaren är på just nu
    private Vector2 lastPosition; //den senaste rutan spelaren var på

    private List<Vector2> lastPositions;
    [SerializeField]
    Tilemap groundTileMap, collisionTileMap;

    public Color selectColor;
    private Color defaultColor;
    // Start is called before the first frame update
    void Start()
    {
        lastPosition = transform.position;
        lastPositions = new List<Vector2>();
        lastPositions.Add(transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            Move(new Vector2(0, 1));
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            Move(new Vector2(0, -1));
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            Move(new Vector2(-1, 0));
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            Move(new Vector2(1, 0));
        }

        if (playerSelected)
        {
            Vector3Int gridPosition = groundTileMap.WorldToCell(transform.position);

            for (int i = 0; i < movementLimit; i++)
            {
                //gå igenom varje ruta från där spelaren står

            }
            groundTileMap.SetColor(gridPosition, selectColor);
            //groundTileMap.color = selectColor;
        }
    }
    void Move(Vector2 direction)
    {
        if (playerSelected)
        {
            if (CanMove(direction))
            {
                Vector2 destination;
                //spara vart spelaren kommer hamna
                destination = transform.position + (Vector3)direction;

                //om spelaren inte kommer hamna på den senaste platsen den var på
                if (destination != lastPositions[lastPositions.Count - 1])
                {
                    //om spelaren inte har rört sig fler steg än den kan under en runda
                    if (currentMovementLength < movementLimit)
                    {
                        //spara spelarens senaste position
                        //lastPosition = transform.position;
                        lastPositions.Add(transform.position);
                        transform.position += (Vector3)direction;
                        //spara spelarens nya position
                        currentPosition = transform.position;
                        //om spelaren inte kommer hamna på den senaste platsen, har den gått ett steg
                        currentMovementLength++;
                    }

                }
                //om spelaren kommer hamna på den senaste platsen den var på
                else
                {
                    //spara spelarens senaste position
                    //lastPosition = transform.position;
                    Vector3Int gridPosition = groundTileMap.WorldToCell(currentPosition);
                    lastPositions.RemoveAt(lastPositions.Count - 1);
                    transform.position += (Vector3)direction;
                    //spara spelarens nya position
                    currentPosition = transform.position;
                    //om spelaren hamnar på den senaste platsen har den gått tillbaka ett steg
                    currentMovementLength--;
                    groundTileMap.SetColor(gridPosition, Color.white);
                }
            }
        }
       
    }
    private bool CanMove(Vector2 direction)
    {
        Vector3Int gridPosition = groundTileMap.WorldToCell(transform.position + (Vector3)direction);
        if (!groundTileMap.HasTile(gridPosition) || collisionTileMap.HasTile(gridPosition))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
