﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    public Stat hp, str, mag, spd, def, res, lck;

    public int currentHealth { get; private set; }
    public int maxHealth;
    private void Awake()
    {
        maxHealth = hp.GetValue();
        currentHealth = maxHealth;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            TakePhysicalDamage(10);
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            TakeMagicDamage(10);
        }

    }
    public void TakePhysicalDamage(int damage)
    {
        //if damage is physical

        damage -= def.GetValue();
        damage = Mathf.Clamp(damage, 0, int.MaxValue); //ser till att skadan inte blir negativ och helar spelaren
        currentHealth -= damage;
        Debug.Log(transform.name + " took " + damage + " damage");
        Debug.Log(transform.name + " has " + currentHealth + " out of " + maxHealth + " left.");
        if (currentHealth <= 0)
        {
            currentHealth = 0;
            Die();
        }
    }
    public void TakeMagicDamage(int damage)
    {
        //if damage is magical

        damage -= res.GetValue();
        damage = Mathf.Clamp(damage, 0, int.MaxValue); //ser till att skadan inte blir negativ och helar spelaren
        currentHealth -= damage;
        Debug.Log(transform.name + " took " + damage + " damage");
        if (currentHealth <= 0)
        {
            currentHealth = 0;
            Die();
        }
        Debug.Log(transform.name + " has " + currentHealth + " out of " + maxHealth + " left.");
    }
    public virtual void Die()
    {

    }
}
