﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfantryObject : CharacterObject
{
    public enum infantryClass
    {
        soldier,
        myrmidon
    }
    public infantryClass characterClass;
    private void Awake()
    {
        type = movementType.infantry;
    }
}
