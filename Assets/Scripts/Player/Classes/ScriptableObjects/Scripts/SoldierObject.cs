﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Infantry Object", menuName = "Characters/Classes/Soldier")]
public class SoldierObject : InfantryObject
{
    private void Awake()
    {
        characterClass = infantryClass.soldier;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
