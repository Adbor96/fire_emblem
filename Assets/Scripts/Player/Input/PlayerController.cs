﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private Tilemap groundTileMap;
    [SerializeField]
    private Tilemap collisionTileMap;
    private PlayerMovement controls;
    private void Awake()
    {
        controls = new PlayerMovement();
    }
    private void OnEnable()
    {
        controls.Enable();
    }
    private void OnDisable()
    {
        controls.Disable();
    }
    // Start is called before the first frame update
    void Start()
    {
        controls.Main.Movement.performed += ContextMenu => Move(ContextMenu.ReadValue<Vector2>());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            Move(new Vector2(0, 1));
        } 
        if (Input.GetKeyDown(KeyCode.S))
        {
            Move(new Vector2(0, -1));
        } 
        if (Input.GetKeyDown(KeyCode.A))
        {
            Move(new Vector2(-1, 0));
        } 
        if (Input.GetKeyDown(KeyCode.D))
        {
            Move(new Vector2(1, 0));
        } 

    }
    void Move(Vector2 direction)
    {
        if (CanMove(direction))
        {
            transform.position += (Vector3)direction;
        }
    }
    private bool CanMove(Vector2 direction)
    {
        Vector3Int gridPosition = groundTileMap.WorldToCell(transform.position + (Vector3)direction);
        if (!groundTileMap.HasTile(gridPosition) || collisionTileMap.HasTile(gridPosition))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
